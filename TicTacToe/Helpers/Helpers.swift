//
//  Helpers.swift
//  TicTacToe
//
//  Created by 2019_DEV_137 on 18/03/2019.
//  Copyright © 2019 2019_DEV_137. All rights reserved.
//

import Foundation

/// Returns a Boolean value indicating whether every element of a sequence
/// is the same.
///
/// - Parameter: The element that you want to check.
/// - Returns: `true` if the sequence only contains the element passed in.
extension Sequence where Element: Equatable {
    func elements(areAll element: Element) -> Bool {
        return allSatisfy { e in
            element == e
        }
    }
}
