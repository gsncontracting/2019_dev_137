//
//  GameBoardModel.swift
//  TicTacToe
//
//  Created by 2019_DEV_137 on 18/03/2019.
//  Copyright © 2019 2019_DEV_137. All rights reserved.
//

import Foundation

enum Result {
    case illegalMove
    case moveMade(GameBoardViewModel)
    case draw(GameBoardViewModel)
    case playerWin(GameBoardViewModel)
}

enum Player: String {
    case playerOne = "Player One"
    case playerTwo = "Player Two"
    
    fileprivate var switchPlayer: Player {
        switch self {
        case .playerOne: return .playerTwo
        case .playerTwo: return .playerOne
        }
    }
    
    var piece: Game.Piece {
        switch self {
        case .playerOne: return .x
        case .playerTwo: return .o
        }
    }
}

struct GameBoardViewModel {
    var game: Game = Game()
    var playerTurn = Player.playerOne
    
    var flattenedBoard: [Game.Piece] {
        return game.board.flatMap { $0 }
    }
    
    static var reset: GameBoardViewModel {
        return GameBoardViewModel(game: .init(), playerTurn: .playerOne)
    }
    
    mutating func update(with coords: Coordinates) -> GameBoardViewModel {
        let board = game.updateBoard(using: coords, playerTurn: playerTurn)
            return GameBoardViewModel(
                game: .init(board: board),
                playerTurn: self.playerTurn)
    }
    
    func swapTurn(current: Player) -> GameBoardViewModel {
        return GameBoardViewModel(
            game: self.game,
            playerTurn: current.switchPlayer)
    }
}

extension GameBoardViewModel {
    
    mutating func process(move playerPiece: Game.Piece, coordinates: Coordinates) -> Result {
        
        guard checkLegalMove(coordinates: coordinates) else {
            return .illegalMove
        }
        
        let updatedModel = update(with: coordinates)
        
        if updatedModel.checkWin(for: playerPiece, coordinates: coordinates) {
            return .playerWin(updatedModel)
        }
        
        if checkDraw(game: game) {
            return .draw(updatedModel)
        }
        
        return .moveMade(updatedModel.swapTurn(current: playerTurn))
    }
    
    func checkLegalMove(coordinates: Coordinates) -> Bool {
        return self.game[coordinates] == .empty
    }
    
    func checkWin(for playerPiece: Game.Piece, coordinates: Coordinates) -> Bool {
        return checkHorizontalWin(for: playerPiece)
            || checkVerticalWin(for: playerPiece)
            || checkDiagonalWin(for: playerPiece)
    }
}
