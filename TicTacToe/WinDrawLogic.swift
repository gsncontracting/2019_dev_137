//
//  CheckingLogic.swift
//  TicTacToe
//
//  Created by 2019_DEV_137 on 18/03/2019.
//  Copyright © 2019 2019_DEV_137. All rights reserved.
//

import Foundation

extension GameBoardViewModel {
    
    func checkHorizontalWin(for playerPiece: Game.Piece) -> Bool {
        for row in game.board {
            if row.elements(areAll: playerPiece) { return true }
        }
        return false
    }
    
    func checkVerticalWin(for playerPiece: Game.Piece) -> Bool {
        for i in 0..<game.board.count {
            
            var match = true
            for row in game.board {
                if row[i] == playerPiece { continue }
                else { match = false; break }
            }
            if match { return true }
        }
        return false
    }
    
    func checkDiagonalWin(for playerPiece: Game.Piece) -> Bool {
        if playerPiece == game.board[0][0] && playerPiece == game.board[1][1] && playerPiece == game.board[2][2] ||
            playerPiece == game.board[2][0] && playerPiece == game.board[1][1] && playerPiece == game.board[0][2] {
            return true
        }
        return false
    }
    
    func checkDraw(game: Game) -> Bool {
        for row in game.board {
            if row.contains(.empty) { return false }
        }
        return true
    }
}
