//
//  GameBoard.swift
//  TicTacToe
//
//  Created by 2019_DEV_137 on 18/03/2019.
//  Copyright © 2019 2019_DEV_137. All rights reserved.
//

import Foundation

struct Coordinates {
    let row: Int
    let column: Int
}

struct Game {
    
    enum Piece: String {
        case empty = "_"
        case x = "X"
        case o = "O"
    }
    
    var board: [[Piece]] = [
        [.empty, .empty, .empty],
        [.empty, .empty, .empty],
        [.empty, .empty, .empty]
    ]
    
    subscript(coordinates: Coordinates) -> Piece {
        get {
            return board[coordinates.row][coordinates.column]
        }
    }
    
    mutating func updateBoard(using coordinates: Coordinates, playerTurn: Player) -> [[Piece]] {
        board[coordinates.row][coordinates.column] = playerTurn.piece
        return board
    }
}
